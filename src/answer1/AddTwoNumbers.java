package answer1;

import java.util.Scanner;

public class AddTwoNumbers {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter two integet numbers: ");
        int num1 = scanner.nextInt();
        int num2 = scanner.nextInt();
        int result = sumOfTwoNumbers(num1,num2);
        System.out.println("Sum of two integer numbers = " + result);
        float answer = sumOfTwoNumbers(12.7f,58.9f);
        System.out.println("Sum of two float numbers = " + answer);
    }

    public static int sumOfTwoNumbers(int num1,int num2){
        return num1 + num2;
    }

    public static float sumOfTwoNumbers(float number1, float number2){
        return number1 + number2;
    }
}
