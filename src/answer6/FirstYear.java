package answer6;

public class FirstYear {
    public static void main(String args[]){
        Faculty[] faculties = new Faculty[6] ;

        faculties[0] = new Faculty(9069,"B.Sc",9.27);
        faculties[1] = new Faculty(9088,"B.Sc",8.5);

        faculties[2] = new Faculty(9054,"M.Sc",7.2);
        faculties[3] = new Faculty(9002,"M.Sc",9.43);

        faculties[4] = new Faculty(9012,"BBA",9.17);
        faculties[5] = new Faculty(9079,"BBA",6.57);

        System.out.println("Students' result:");
        faculties[0].bestStudent();
        faculties[1].bestStudent();
        faculties[2].bestStudent();
        faculties[3].bestStudent();
        faculties[4].bestStudent();
        faculties[5].bestStudent();
    }
}
class Faculty{
    int studentId;
    String nameOfClass;
    double gpa;
    Faculty(int studentId, String nameOfClass,double gpa){
        this.studentId = studentId;
        this.nameOfClass = nameOfClass;
        this.gpa = gpa;
    }
    public void bestStudent(){
        if(this.gpa>9){
            System.out.println(this.studentId + " is the best one of this school,\n" +
                    "He/She took " + this.gpa + " mark out of 10");
        }else{
            System.out.println(this.studentId + " is not bad!\n" +
                    "because he/she took " + this.gpa);
        }
    }
}
