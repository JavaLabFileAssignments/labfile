package answer9;

public class Swapping {
    String model;
    int price;

    Swapping(String model, int price)
    {
        this.model = model;
        this.price = price;
    }

    void print()
    {
        System.out.println("price = " + this.price + ", model = " + this.model);
    }
}

class CarWrapper
{
    Swapping swapping;

    CarWrapper(Swapping swapping){
        this.swapping = swapping;
    }
}

class Main
{
    public static void swap(CarWrapper carWrapper, CarWrapper carWrapper1)
    {
        Swapping temp = carWrapper.swapping;
        carWrapper.swapping = carWrapper1.swapping;
        carWrapper1.swapping = temp;
    }

    public static void main(String[] args)
    {
        Swapping firstObject = new Swapping("Audi", 120000);
        Swapping secondObject = new Swapping("BMW", 90000);
        CarWrapper carWrapper = new CarWrapper(firstObject);
        CarWrapper carWrapper1 = new CarWrapper(secondObject);
        swap(carWrapper, carWrapper1);
        carWrapper.swapping.print();
        carWrapper1.swapping.print();
    }
}
