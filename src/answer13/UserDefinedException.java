package answer13;

public class UserDefinedException extends Exception{
        private String message;

        UserDefinedException(String message) {
            this.message = message;
        }
        public String toString(){
            return ("MyException executed: " + this.message) ;
        }
}

class Main{
    public static void main(String args[]){
        try{
            System.out.println("Starting with try block");
            // I'm throwing the custom exception using throw
            throw new UserDefinedException("This is my error message");
        }
        catch(UserDefinedException exception){
            System.out.println("Catch Block") ;
            System.out.println(exception) ;
        }
    }
}

