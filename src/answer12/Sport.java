package answer12;

import java.util.Scanner;

interface Ticket{
    int price = 0;
    void counting();;
}

interface Football extends Ticket{
     void play();
     void goal();
     void team();
}

interface Swimming extends Ticket{
     String swim();
     void competition();
}

interface Hockey extends Ticket{
     void homeGoalScored();
     void visitingGoalScored();
     void overtimePeriod(int ot);
}

public interface Sport extends Football,Swimming,Hockey{
     void setHomeTeam(String name);
     void setVisitingTeam(String name);
}

class Performance implements Sport{

    boolean goal;
    Scanner scanner = new Scanner(System.in);

    @Override
    public void counting() {
        System.out.println("There are executing how many tickets bought");
    }

    @Override
    public void play() {
        //there are going on some process about playing football
    }

    @Override
    public void goal() {
        if (goal==true){
            System.out.println("Goooaaalllll!!!!!!!!!");
        }else{
            System.out.println("Go'l bolishiga sal qold)))");
        }
    }

    @Override
    public void team() {
        //writing about teams members
    }

    @Override
    public String swim() {
        return null;
    }

    @Override
    public void competition() {
        //When how where will be competition, give information with this method
    }

    @Override
    public void homeGoalScored() {
        goal = scanner.nextBoolean();
        if(goal==true){
            System.out.println("Home team score goal!!");
        }else{
            System.out.println("Guest team score goal!!");
        }
    }

    @Override
    public void visitingGoalScored() {
        //it is doing same process like above method
    }

    @Override
    public void overtimePeriod(int ot) {
        //it gives information if game is over
    }

    @Override
    public void setHomeTeam(String name) {
        //...............
    }

    @Override
    public void setVisitingTeam(String name) {
        //..............
    }
}

class Main{
    public static void main(String[] args) {
        Performance sports = new Performance();
        sports.goal();
        sports.counting();
        sports.homeGoalScored();
    }
}