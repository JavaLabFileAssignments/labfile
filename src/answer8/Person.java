package answer8;

public class Person {

    private String name;
    private int age;
    private double salary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}

class Main{
    public static void main(String[] args) {
        Person person = new Person();
        person.setName("Muzammil");
        person.setAge(20);
        person.setSalary(12000.5);
        System.out.println("My name is " + person.getName() +
                "and I am " + person.getAge() + " years old " +
                "\nso I take " + person.getSalary() + "$ per month....");
    }
}
