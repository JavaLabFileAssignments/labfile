package answer7;

abstract class Shape {
    public abstract void area(int length,int width);
}

class Rectangle extends Shape{

    @Override
    public void area(int length, int width) {
        int answer = length * width;
        System.out.println("Rectangle = " + answer);
    }
}

class Triangle extends Shape{

    @Override
    public void area(int length, int width) {
        int answer = length * width / 2;
        System.out.println("Triangle = " + answer);
    }
}

class Circle extends Shape{

    private static final double PI = 3.14;

    @Override
    public void area(int length, int width) {
        double answer = length * length * PI;
        System.out.println("Circle = " + answer);
    }
}

class Main{
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        rectangle.area(12,4);
        Triangle triangle  = new Triangle();
        triangle.area(16,4);
        Circle circle = new Circle();
        circle.area(5,2);
    }
}