package answer5;

public class MainClass {
    static Student[] students = new Student[3];
    public static void main(String[] args) {
        Student student1 = new Student(9069,"Tokhirkhuja",90.0,80.5,70.0);
        Student student2 = new Student(9060,"Ibrohim",92.4,82.2,71.5);
        Student student3 = new Student(9066,"Dilshod",91.2,70.4,95.0);

        students[0] = student1;
        students[1] = student2;
        students[2] = student3;
        System.out.println("About first student:");
        students[0].showAllDetails();
        System.out.println("About second student:");
        students[1].showAllDetails();
        System.out.println("About third student:");
        students[2].showAllDetails();
    }
}
