package answer5;

public class Student {

    private int enrollmentNo;
    private String name;
    private double markOfSubject1;
    private double markOfSubject2;
    private double markOfSubject3;
    private double totalMarks;

    public Student(int enrollmentNo, String name, double markOfSubject1, double markOfSubject2, double markOfSubject3) {
        this.enrollmentNo = enrollmentNo;
        this.name = name;
        this.markOfSubject1 = markOfSubject1;
        this.markOfSubject2 = markOfSubject2;
        this.markOfSubject3 = markOfSubject3;
        if(this.markOfSubject1<50.0||this.markOfSubject2<50.0||this.markOfSubject3<50.0){
            this.totalMarks = 0.0;
        }
    }

    private double passExam(){
        if(this.markOfSubject1>=50.0&&this.markOfSubject2>=50.0&&this.markOfSubject3>=50.0){
            this.totalMarks = this.markOfSubject1 + this.markOfSubject2 + this.markOfSubject3;
        }
        return this.totalMarks;
    }

    public void showAllDetails(){
        System.out.println("Name = " + this.name +"\nEnrollmentNo = " + this.enrollmentNo +
                 "\nTotalMarks = " + passExam());
    }
}
