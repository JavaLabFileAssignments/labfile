package answer11;

public class ThreadChangeName extends Thread  {

        public void run(){
            for(int i=1;i<4;i++)   {
                try  {
                    //call sleep method of thread
                    Thread.sleep(500);
                }catch(InterruptedException exception){
                    System.out.println(exception);
                }
                //print current thread instance with loop variable
                System.out.println(Thread.currentThread().getName() + "   : " + i);
            }
        }
    }

    class Main{
        public static void main(String args[])
        {
            ThreadChangeName firstthread = new ThreadChangeName();
            ThreadChangeName secondthread = new ThreadChangeName();
            //start threads one by one
            firstthread.start();
            secondthread.start();
        }
    }

