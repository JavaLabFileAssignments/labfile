package answer2;

public class DifferectConstructor {
    private String name;
    private int age;
    private double gpa;
    private int phoneNo;

    public DifferectConstructor(String name, int age, double gpa, int phoneNo) {
        this(name, age, gpa);
        this.phoneNo = phoneNo;
    }

    public DifferectConstructor(String name, int age, double gpa) {
        this(name, age);
        this.gpa = gpa;
    }

    public DifferectConstructor(String name, int age) {
        this(name);
        this.age = age;
    }

    public DifferectConstructor(String name, double gpa) {
        this(name);
        this.gpa = gpa;
    }

    public DifferectConstructor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public double getGpa() {
        return gpa;
    }

    public int getPhoneNo() {
        return phoneNo;
    }
}

class Main{
    public static void main(String[] args) {
        DifferectConstructor constructor =
                new DifferectConstructor("Tohir",9.27);
        System.out.println("My name is " + constructor.getName() +
                " and My gpa = " + constructor.getGpa());

        //We are create an object of class with another constructor
        DifferectConstructor differectConstructor =
                new DifferectConstructor("Muzammil");
    }
}
