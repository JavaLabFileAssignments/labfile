package answer4;

public class StaticVariable{
    static int count=0;
    public void increment()
    {
        count++;
    }
    public static void main(String args[])
    {
        StaticVariable variable = new StaticVariable();
        StaticVariable variable1 = new StaticVariable();
        variable.increment();
        variable1.increment();
        System.out.println("First object: count is = " + variable.count);
        System.out.println("Second object: count is = " + variable1.count);
    }
}
