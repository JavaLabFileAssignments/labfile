package answer3;

public class AddSub {
    private int firstNumber;
    private int secondNumber;

    public AddSub(int firstNumber, int secondNumber) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }

    public void setFirstNumber(int firstNumber) {
        this.firstNumber = firstNumber;
    }

    public void setSecondNumber(int secondNumber) {
        this.secondNumber = secondNumber;
    }

    public int getFirstNumber() {
        return firstNumber;
    }

    public int getSecondNumber() {
        return secondNumber;
    }

    public void add(){
        int adding = firstNumber + secondNumber;
        System.out.println("The answer of adding two numbers = " + adding);
    }

    public void subtract(){
        int subtracting;
        if(firstNumber>secondNumber){
            subtracting = firstNumber - secondNumber;
        }else{
            subtracting = secondNumber - firstNumber;
        }
        System.out.println("The answer of subtracting two numbers = " + subtracting);
    }
}
