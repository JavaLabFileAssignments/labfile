package answer3;

public class MulDiv extends AddSub{
    private int firstNumber;
    private int secondNumber;

    public MulDiv(int firstNumber, int secondNumber) {
        super(firstNumber, secondNumber);
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }

    public void multiply (){
        int multiplying = firstNumber * secondNumber;
        System.out.println("The answer of multiplying two numbers = " + multiplying);
    }

    public void divide(){
        int dividing;
        if(firstNumber>secondNumber){
            dividing = firstNumber / secondNumber;
        }else{
            dividing = secondNumber / firstNumber;
        }
        System.out.println("The answer of dividing two numbers = " + dividing);
    }
}
