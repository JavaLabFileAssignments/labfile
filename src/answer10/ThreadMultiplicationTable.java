package answer10;

import java.util.Scanner;

public class ThreadMultiplicationTable {
    static Scanner key = new Scanner(System.in);
    public static void main(String[] args) throws InterruptedException {
        System.out.print("Enter a number: ");
        int number = key.nextInt();
        Thread multiplicationTable1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=1;i<=10;i++){
                    System.out.println(number + " * " + i + " = " + (number * i));
                }
            }
        },"First Thread");

        Thread multiplicationTable2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=1;i<=10;i++){
                    System.out.println((number +1)+ " * " + i + " = " + ((number+1) * i));
                }
            }
        },"Second Thread");

        Thread multiplicationTable3 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=1;i<=10;i++){
                    System.out.println((number-1) + " * " + i + " = " + ((number-1)* i));
                }
            }
        },"Thrid Thread");

        System.out.println(multiplicationTable1.getName() + " is running");

        multiplicationTable1.start();

        try {
            Thread.sleep(20);
        }catch (Exception exception){

        }

        System.out.println(multiplicationTable2.getName() + " is running");

        multiplicationTable2.start();

        try {
            Thread.sleep(20);
        }catch (Exception exception){

        }

        System.out.println(multiplicationTable3.getName() + " is running");

        multiplicationTable3.start();

        multiplicationTable1.join();
        multiplicationTable2.join();
        multiplicationTable3.join();

        System.out.println("Threads are ended....");
    }
}
